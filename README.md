AsteroidRemover

By S Max

Remove blasted asteroids between save games.
Reset after witchspace jump.

Available in [Oolite](http://www.oolite.org/) package manager (Mechanics).

# License
This work is licensed under the Creative Commons Attribution-Noncommercial-Share Alike 4.0 Unported License. To view a copy of this license, visit http://creativecommons.org/licenses/by-nc-sa/4.0/

## Version History
## [0.3] - 2016-08-24

- Bugfix (OXP fails when destroyed all asteroids)
- Improved compatibility with other plugins

## [0.2] - 2016-08-11

- New way to remove asteroids
- Improved compatibility with other plugins

## [0.1] - 2016-08-10

- Initial release