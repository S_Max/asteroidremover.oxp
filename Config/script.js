/* global missionVariables system log*/

this.name = "AsteroidRemover";
this.author = "SMax";
this.copyright = "2016 SMax";
this.licence = "CC-BY-NC-SA 4.0";
this.description = "Remove blasted asteroids between save games. Reset after witchspace jump.";
this.version = "0.3";

"use strict";

this._DEBUG = false;
this._SHOW_DATA = false;

this._StoredShipsID = [];
this._StoredShipsNames = [];

this.startUpComplete = function() {
	this._InitAsteroids();

	if (this._SHOW_DATA) {
		this._logger("LOAD AsteroidRemover_DATA: " + missionVariables.AsteroidRemover_DATA);
	}
	var ships = JSON.parse(missionVariables.AsteroidRemover_DATA);
	if (!ships) {
		ships = [];
	}

	var t = system.shipsWithPrimaryRole("asteroid");

	if (t) {
		this._logger("LOAD Asteroids in system: " + t.length);
		for (var i = 0; i < t.length; i++) {
			if (this._testShip(t[i])) {
				var name = this._getShipName(t[i], t[i].position);
				if (ships.indexOf(name) != -1) {
					t[i].remove();
					this._logger("REMOVE: " + name);
				}
			}
		}
	}
};

this.playerWillSaveGame = function() {

	var t = system.shipsWithPrimaryRole("asteroid");

	var ships = [];

	if (t) {
		this._logger("SAVE Asteroids in system: " + t.length);
		for (var i = 0; i < t.length; i++) {
			if (this._testShip(t[i])) {
				var ID = this._getShipID(t[i]);
				ships.push(ID);
			}
		}
	}

	var toSaveData = [];

	for (var i = 0; i < this._StoredShipsID.length; i++) {
		var ID = this._StoredShipsID[i];
		if (ships.indexOf(ID) == -1) {
			var name = this._StoredShipsNames[i];
			toSaveData.push(name);
			this._logger("SAVE " + name);
		}
	}

	missionVariables.AsteroidRemover_DATA = JSON.stringify(toSaveData);
	if (this._SHOW_DATA) {
		this._logger("SAVE AsteroidRemover_DATA: " + missionVariables.AsteroidRemover_DATA);
	}
};

this.shipExitedWitchspace = function() {
	this._InitAsteroids();
};

this._logger = function(msg) {
	if (this._DEBUG) {
		log(this.name, msg);
	}
};

this._testShip = function(ship) {
	return ship.isShip && ship.AI == "dumbAI.plist";
};

this._InitAsteroids = function() {
	this._StoredShipsID = [];
	this._StoredShipsNames = [];
	var t = system.shipsWithPrimaryRole("asteroid");

	if (t) {
		this._logger("INIT Asteroids in system: " + t.length);
		for (var i = 0; i < t.length; i++) {
			if (this._testShip(t[i])) {
				var ID = this._getShipID(t[i], t[i].position);
				var name = this._getShipName(t[i], t[i].position);

				this._StoredShipsID.push(ID);
				this._StoredShipsNames.push(name);

				this._logger("INIT " + name);
			}
		}
	}
};

this._getShipID = function(ship, position) {
	return ship.entityPersonality;
};
this._getShipName = function(ship, position) {
	var name = {
		k: ship.dataKey,
		p: position.toArray()
	};

	return JSON.stringify(name);
};